.. delorean documentation master file, created by
   sphinx-quickstart on Wed Oct 24 06:57:18 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue dans la documentation
===============================

C'est tout de même mieux qu'un doc word non?


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules
   delorean


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
